describe('gameoflife', function()
  local game = require 'gameoflife'()
  it('setBoard will set grid with one live cell', function()
    game:setBoard{
      { 0, 0, 0 },
      { 0, 1, 0 },
      { 0, 0, 0 }
    }
    assert.is_true(game:isAlive(2,2))
    assert.is_false(game:isAlive(1,1))
  end)
  it('nextGen when 1 live cell has 0 live neighbors it will die', function()
    game:setBoard{
      { 0, 0, 0 },
      { 0, 1, 0 },
      { 0, 0, 0 }
    }
    assert.is_true(game:isAlive(2,2))
    game:nextGen()
    assert.is_false(game:isAlive(2,2))
  end)
  it('nextGen when 1 live cell has 2 live neighbor it will survive', function()
    game:setBoard{
      { 0, 0, 0 },
      { 1, 1, 1 },
      { 0, 0, 0 }
    }
    assert.is_true(game:isAlive(2,2))
    game:nextGen()
    assert.is_true(game:isAlive(2,2))
  end)
  it('nextGen when 1 dead cell has 3 live neighbor it will revive', function()
    game:setBoard{
      { 0, 1, 0 },
      { 1, 0, 0 },
      { 0, 1, 0 }
    }
    assert.is_false(game:isAlive(2,2))
    game:nextGen()
    assert.is_true(game:isAlive(2,2))
  end)
  it('nextGen when 1 live cell has > 3 live neighbor it will die of overpopulation', function()
    game:setBoard{
      { 0, 1, 0 },
      { 1, 1, 1 },
      { 0, 1, 0 }
    }
    assert.is_true(game:isAlive(2,2))
    game:nextGen()
    assert.is_false(game:isAlive(2,2))
  end)
end)
