local function isAlive(self, row, col)
  if self.board[row] == nil then return false end
  return self.board[row][col] == 1
end

local function setBoard(self, board)
  self.board = board
end

local function makeAlive(b, r, c)
   if b[r] == nil then b[r] = {} end
   b[r][c] = 1  
end
local function nbrAliveCount(self, r,c)
  local count = 0
  for i = -1,1 do
    for j = -1,1 do
      if (i ~= 2 or j ~=2) and self:isAlive(r+i,c+j) then
        count = count + 1
      end
    end
  end
  return count
end

local function nextGen(self)
  local newBoard = {}
  for rowindex,row in pairs(self.board) do
    for colindex,_ in pairs(row) do
      local nbrAliveCount = self:nbrAliveCount(rowindex, colindex)
      if self:isAlive(rowindex, colindex) then
        if nbrAliveCount >= 2 and nbrAliveCount <= 3 then
          makeAlive(newBoard, rowindex, colindex)
        end
      else
        if nbrAliveCount == 3 then
          makeAlive(newBoard, rowindex, colindex)
        end
      end
    end
  end
  self.board = newBoard
end

return function()
  return {
    setBoard = setBoard,
    isAlive = isAlive,
    nbrAliveCount = nbrAliveCount,
    nextGen = nextGen,
  }
end
